import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {
	/*
	Test Case 1:
	Creating a Product
	When you create the product, it has a title and a price
	
	Test Case 2:
	Checking if two products are equal
	Two products are equal if they have the same name.
	*/


	@Test
	public void testCreateProduct() 
	{
		
		Product p=new Product("chair",40);
		
		assertEquals("chair",p.getTitle());
		
		assertEquals(40,p.getPrice(),0.01);
	}
	
	@Test
	public void testTwoProductsEqual() 
	{
		Product p1=new Product("shoes",25);
		Product p2=new Product("shoes",20);
		
		assertEquals(true,p1.equals(p2));
	}

}
