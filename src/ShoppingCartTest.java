import static org.junit.Assert.*;

import org.junit.Test;

public class ShoppingCartTest {
	/*
	 Creating a cart
	When created, the cart has 0 items 
	
	Empty Carts
	When empty, the cart has 0 items  
	
	Adding Products to Cart
	When a new product is added, the number of items must be incremented  
	When a new product is added, the new balance must be the sum of the previous balance plus the cost of the new product 
	 
	Removing Products from Cart
	When an item is removed, the number of items must be decreased  
	When a product not in the cart is removed, a ProductNotFoundException must be thrown  Hint: insert the call in a try block and put a fail() after the call to removeItem()

	 */


	@Test
	public void testCreateCart() 
	{
		// When created, the cart has 0 items 
		// 1. Create a shopping cart
		ShoppingCart cart=new ShoppingCart();
		
		// 2. Use getItemCount() to check how many items are in cart
		int numItems= cart.getItemCount();
		
		assertEquals(0,numItems);
		
	}

	@Test
	public void testEmptyCart() 
	{
		ShoppingCart cart=new ShoppingCart();
		cart.addItem(new Product("coffee",200));
		cart.addItem(new Product("donut",100));
		cart.empty();
		assertEquals(0,cart.getItemCount());
		
	}
	
	@Test
	public void testAddingProductsToCart() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testRemoveProductFromCart() {
		fail("Not yet implemented");
	}

}
